﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CommandFramework
{
    public class CommandFramework
    {
        private const string paramsRegex = "^(?<command>[a-zA-Z0-9_]+) (?<parameters>(?<parameterName>-[[a-zA-Z0-9]+ )*(?:(?<parameterValue>[a-zA-Z0-9]+)|'(?<parameterValue>[a-zA-Z0-9 ]+)'|\"(?< parameterValue >[a - zA - Z0 - 9] +)\")[ ]*)*$";

        public string ProcessCommand(string commandWithParameters)
        {
            try
            {
                Command command = new Command(commandWithParameters);
                MethodInfo[] methods = this.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance);

                foreach (MethodInfo method in methods)
                {
                    if (method.Name.ToLower() == command.CommandName)
                    {
                        ParameterInfo[] methodParameters = method.GetParameters();

                        List<object> parametersToSend = new List<object>();

                        foreach (ParameterInfo parameter in methodParameters)
                        {
                            string parameterName = parameter.Name.ToLower();
                            if (command.NamedParameters.ContainsKey(parameterName))
                            {
                                Type parameterType = parameter.ParameterType;
                                object parameterValue = TypeDescriptor.GetConverter(parameterType).ConvertFromInvariantString(command.NamedParameters[parameterName]);
                                parametersToSend.Add(parameterValue);
                            }
                            else if (parameter.IsOptional)
                            {
                                parametersToSend.Add(parameter.DefaultValue);
                            }
                            else if (parameter.GetCustomAttribute(typeof(ParamArrayAttribute), false) != null && command.UnnamedParameters.Count > 0)
                            {
                                Type parameterType = parameter.ParameterType.GetElementType();
                                var genparameters = new List<parameterType>();
                                foreach (string genparameter in command.UnnamedParameters)
                                {
                                    var parameterValue = TypeDescriptor.GetConverter(parameterType).ConvertFromInvariantString(genparameter);
                                    genparameters.Add(parameterValue);
                                }
                                parametersToSend.Add(genparameters.ToArray());
                            }
                            else
                                continue;
                        }

                        if (parametersToSend.Count != methodParameters.Length)
                            continue;

                        method.Invoke(this, parametersToSend.ToArray());
                        return null;
                    }
                }
                throw new Exception("No command found that matches the specified signature.");
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
