﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CommandFramework
{
    public class Command
    {
        private const string commandRegex = "^(?<Command>[a-zA-Z0-9_]+)[ ]?";
        private const string parameterRegex = "(?:-(?<ParameterName>[A-Za-z0-9]+) )?(?:(?<ParameterValue>[A-Za-z0-9]+)|'(?<ParameterValue>[A-Za-z0-9 ]+)'|\"(?<ParameterValue>[A-Za-z0-9 ]+)\")[ ]?";

        public string CommandName { get; set; }
        public Dictionary<string, string> NamedParameters { get; set; }
        public List<string> UnnamedParameters { get; set; }

        public Command(string commandString)
        {
            CommandName = parseCommandName(commandString);
            NamedParameters = new Dictionary<string, string>();

            string parameters = Regex.Replace(commandString, commandRegex, "");
            parseParameters(parameters);
        }

        private string parseCommandName(string input)
        {
            Match match = Regex.Match(input, commandRegex);
            if (!match.Success)
                throw new Exception("Unable to parse command.");

            return match.Groups["Command"].Value.ToLower();
        }

        private void parseParameters(string input)
        {
            NamedParameters = new Dictionary<string, string>();
            UnnamedParameters = new List<string>();

            while (input.Length > 0)
            {
                string param = Regex.Match(input, parameterRegex).Value;
                input = input.Replace(param, "");
                var parameter = parseParameter(param);
                if (parameter.Key.Length > 0)
                {
                    NamedParameters.Add(parameter.Key, parameter.Value);
                }
                else
                {
                    UnnamedParameters.Add(parameter.Value);
                }
            }
        }

        private KeyValuePair<string, string> parseParameter(string input)
        {
            Match match = Regex.Match(input, parameterRegex);
            if (!match.Success)
                throw new Exception("Unable to parse parameter.");

            string parameterName = match.Groups["ParameterName"].Value.ToLower();
            string parameterValue = match.Groups["ParameterValue"].Value;

            if (parameterValue.Length <= 0)
                throw new Exception(string.Format("No value for the parameter '{0}'.", parameterName));

            return new KeyValuePair<string, string>(parameterName, parameterValue);
        }
    }
}
