﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCommandFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            Commands commands = new Commands();

            while (!commands.ExitRequested)
            {
                Console.Write(">");
                string command = Console.ReadLine();

                string commandResult = commands.ProcessCommand(command);

                if (commandResult != null)
                    Console.WriteLine(commandResult);
            }
        }
    }
}
