﻿/// Commands may have optional parameters.
/// Command names must be unique case insensitive.
/// Command parameter names must be unique case insensitive.

using System;

namespace SimpleCommandFramework
{
    class Commands : CommandFramework.CommandFramework
    {
        public bool ExitRequested { get; set; }

        public Commands() : base()
        {
            ExitRequested = false;
        }

        public void Test()
        {
            Console.WriteLine("This is a test command.");
        }

        public void ParameterTest(string input)
        {
            Console.WriteLine("EchoTest: " + input);
        }

        public void OptionalTest(string input = "default")
        {
            Console.WriteLine("OptionalTest: " + input);
        }

        public void Add(params int[] values)
        {
            int result = values[0];
            string message = values[0].ToString();

            for (int i = 1; i < values.Length; i++)
            {
                result += values[i];
                message += " + " + values[i];
            }

            message += " = " + result;
            Console.WriteLine(message);
        }

        public void Quit()
        {
            ExitRequested = true;
        }
    }
}
